import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class CommandKey extends JFrame implements KeyListener {
	JLabel jLabel;
	
	CommandKey() {
		JLabel jLabel = new JLabel("Command + Wを押すと……?");
		this.add(jLabel);
		this.setSize(200, 200);
		this.setVisible(true);
		this.addKeyListener(this);
	}
	
	public static void main(String[] args) {
		new CommandKey();
	}

	@Override
	public void keyTyped(KeyEvent e) {
			
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keycode = e.getKeyCode();
		int mod = e.getModifiersEx();

		if ((mod & InputEvent.META_DOWN_MASK) != 0) {
			if (keycode == KeyEvent.VK_W) {
				System.exit(0);
			} else if (keycode == KeyEvent.VK_A) {
				System.out.println("!!!!!!!!!");
			}
		}

	}
	

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
